export * from './lib/backends.module';
export * from './lib/hero/hero';
export * from './lib/hero/hero.service';
export * from './lib/data/in-memory-data.service';
