import { async, TestBed } from '@angular/core/testing';
import { BackendsModule } from './backends.module';

describe('BackendsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BackendsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(BackendsModule).toBeDefined();
  });
});
