module.exports = {
  name: 'features',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/features'
};
