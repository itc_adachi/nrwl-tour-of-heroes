import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    RouterModule.forChild([
      {path: ':id', pathMatch: 'full', component: HeroDetailComponent }
    ])
  ],
  declarations: [HeroDetailComponent]
})
export class ContentsHeroDetailModule {}
