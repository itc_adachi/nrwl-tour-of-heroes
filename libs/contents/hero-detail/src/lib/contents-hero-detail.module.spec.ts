import { async, TestBed } from '@angular/core/testing';
import { ContentsHeroDetailModule } from './contents-hero-detail.module';

describe('ContentsHeroDetailModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ContentsHeroDetailModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ContentsHeroDetailModule).toBeDefined();
  });
});
