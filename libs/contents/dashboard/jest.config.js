module.exports = {
  name: 'contents-dashboard',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/contents/dashboard'
};
