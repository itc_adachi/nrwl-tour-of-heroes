import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentsOrganismsModule } from '@tour-of-heroes/components/organisms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StoresHeroesModule } from '@tour-of-heroes/stores/heroes';

@NgModule({
  imports: [
    CommonModule,
    ComponentsOrganismsModule,
    StoresHeroesModule,

    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: DashboardComponent }
    ])
  ],
  declarations: [DashboardComponent]
})
export class ContentsDashboardModule {}
