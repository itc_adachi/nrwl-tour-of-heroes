import { Component, OnInit } from '@angular/core';
import { HeroService } from '@tour-of-heroes/backends';
import { Hero } from '@tour-of-heroes/backends';
import { Observable, Subject } from 'rxjs';

import { HeroesFacade, Entity } from '@tour-of-heroes/stores/heroes';
import { map } from 'rxjs/operators';

import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'tour-of-heroes-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  loaded$: Observable<boolean>;
  topHeroes$: Observable<Entity[]>;

  constructor(private heroesFacade: HeroesFacade, private heroService: HeroService) {
    this.loaded$ = heroesFacade.loaded$;
    this.topHeroes$ = heroesFacade.allHeroes$.pipe(
      map(heroes => heroes.slice(1, 5))
    );
  }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroesFacade.loadAll();
  }
}
