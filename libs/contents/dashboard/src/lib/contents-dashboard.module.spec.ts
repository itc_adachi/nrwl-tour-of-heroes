import { async, TestBed } from '@angular/core/testing';
import { ContentsDashboardModule } from './contents-dashboard.module';

describe('ContentsDashboardModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ContentsDashboardModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ContentsDashboardModule).toBeDefined();
  });
});
