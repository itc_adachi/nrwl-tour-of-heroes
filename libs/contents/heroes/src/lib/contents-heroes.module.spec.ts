import { async, TestBed } from '@angular/core/testing';
import { ContentsHeroesModule } from './contents-heroes.module';

describe('ContentsHeroesModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ContentsHeroesModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ContentsHeroesModule).toBeDefined();
  });
});
