import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component';
import { ComponentsOrganismsModule } from '@tour-of-heroes/components/organisms';

import { StoresHeroesModule } from '@tour-of-heroes/stores/heroes';

@NgModule({
  imports: [
    CommonModule,
    ComponentsOrganismsModule,
    StoresHeroesModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: HeroesComponent }
    ])
  ],
  declarations: [HeroesComponent]
})
export class ContentsHeroesModule {}
