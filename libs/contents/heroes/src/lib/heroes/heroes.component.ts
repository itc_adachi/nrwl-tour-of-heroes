import { Component, OnInit, assertPlatform } from '@angular/core';
import { Hero } from '@tour-of-heroes/backends';
import { HeroService } from '@tour-of-heroes/backends';

import { HeroesFacade, Entity } from '@tour-of-heroes/stores/heroes';
import { Observable } from 'rxjs';

@Component({
  selector: 'tour-of-heroes-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

  heroes: Hero[];
  loaded$: Observable<boolean>;
  allHeroes$: Observable<Entity[]>;
  selectedHeroes$: Observable<Entity>;

  constructor(private heroesFacade: HeroesFacade, private heroService: HeroService) {
    this.loaded$ = heroesFacade.loaded$;
    this.allHeroes$ = heroesFacade.allHeroes$;
    this.selectedHeroes$ = heroesFacade.selectedHeroes$;
  }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroesFacade.loadAll();
  }

  addHero($event): void {
    const name: string = $event.name.trim();
    if (!name) { return; }
    // this.heroService.addHero({ name } as Hero).subscribe(
    //   () => this.getHeroes()
    // );
    this.heroesFacade.addHero({ name } as Hero);
  }

  deleteHero($event): void {
    this.heroesFacade.deleteHero($event.hero);
    // this.heroService.deleteHero($event.hero).subscribe(
    //   () => this.getHeroes()
    // );
  }
}
