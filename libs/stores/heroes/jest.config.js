module.exports = {
  name: 'stores-heroes',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/stores/heroes'
};
