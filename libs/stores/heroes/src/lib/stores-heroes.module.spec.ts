import { async, TestBed } from '@angular/core/testing';
import { StoresHeroesModule } from './stores-heroes.module';

describe('StoresHeroesModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoresHeroesModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(StoresHeroesModule).toBeDefined();
  });
});
