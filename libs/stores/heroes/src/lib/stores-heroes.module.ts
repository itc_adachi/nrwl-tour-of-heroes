import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  HEROES_FEATURE_KEY,
  initialState as heroesInitialState,
  heroesReducer
} from './+state/heroes.reducer';
import { HeroesEffects } from './+state/heroes.effects';
import { HeroesFacade } from './+state/heroes.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(HEROES_FEATURE_KEY, heroesReducer, {
      initialState: heroesInitialState
    }),
    EffectsModule.forFeature([HeroesEffects])
  ],
  providers: [HeroesFacade]
})
export class StoresHeroesModule {}
