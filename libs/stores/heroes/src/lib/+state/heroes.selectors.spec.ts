import { Entity, HeroesState } from './heroes.reducer';
import { heroesQuery } from './heroes.selectors';

describe('Heroes Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getHeroesId = it => it['id'];

  let storeState;

  beforeEach(() => {
    const createHeroes = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
    storeState = {
      heroes: {
        list: [
          createHeroes('PRODUCT-AAA'),
          createHeroes('PRODUCT-BBB'),
          createHeroes('PRODUCT-CCC')
        ],
        selectedId: 'PRODUCT-BBB',
        error: ERROR_MSG,
        loaded: true
      }
    };
  });

  describe('Heroes Selectors', () => {
    it('getAllHeroes() should return the list of Heroes', () => {
      const results = heroesQuery.getAllHeroes(storeState);
      const selId = getHeroesId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelectedHeroes() should return the selected Entity', () => {
      const result = heroesQuery.getSelectedHeroes(storeState);
      const selId = getHeroesId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getLoaded() should return the current 'loaded' status", () => {
      const result = heroesQuery.getLoaded(storeState);

      expect(result).toBe(true);
    });

    it("getError() should return the current 'error' storeState", () => {
      const result = heroesQuery.getError(storeState);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
