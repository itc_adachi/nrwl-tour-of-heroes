import { Action } from '@ngrx/store';
import { Entity } from './heroes.reducer';

export enum HeroesActionTypes {
  LoadHeroes = '[Heroes] Load Heroes',
  HeroesLoaded = '[Heroes] Heroes Loaded',
  HeroesLoadError = '[Heroes] Heroes Load Error',
  AddHero = '[Heroes] Add Hero',
  HeroAddedError = '[Heroes] Hero Added Error',
  DeleteHero = '[Heroes] Delete Hero',
  HeroDeletedError = '[Heroes] Hero Deleted Error',
  SearchHeroes = '[Heroes] Search Hero',
  HeroesSearched = '[Heroes] Hero Searched',
  HeroesSearchedError = '[Heroes] Hero Searched Error',
}

export class LoadHeroes implements Action {
  readonly type = HeroesActionTypes.LoadHeroes;
}

export class HeroesLoadError implements Action {
  readonly type = HeroesActionTypes.HeroesLoadError;
  constructor(public payload: any) {}
}

export class HeroesLoaded implements Action {
  readonly type = HeroesActionTypes.HeroesLoaded;
  constructor(public payload: Entity[]) {}
}

export class AddHero implements Action {
  readonly type = HeroesActionTypes.AddHero;
  constructor(public payload: Entity) {}
}

export class HeroAddedError implements Action {
  readonly type = HeroesActionTypes.HeroAddedError;
  constructor(public payload: any) {}
}

export class DeleteHero implements Action {
  readonly type = HeroesActionTypes.DeleteHero;
  constructor(public payload: Entity) {}
}

export class HeroDeletedError implements Action {
  readonly type = HeroesActionTypes.HeroDeletedError;
  constructor(public payload: any) {}
}

export class SearchHeroes implements Action {
  readonly type = HeroesActionTypes.SearchHeroes;
  constructor(public payload: string) {}
}

export class HeroesSearched implements Action {
  readonly type = HeroesActionTypes.HeroesSearched;
  constructor(public payload: Entity[]) {}
}

export class HeroesSearchedError implements Action {
  readonly type = HeroesActionTypes.HeroesSearchedError;
  constructor(public payload: any) {}
}

export type HeroesAction =
  LoadHeroes | HeroesLoaded | HeroesLoadError |
  AddHero | HeroAddedError |
  DeleteHero | HeroDeletedError |
  SearchHeroes | HeroesSearched | HeroesSearchedError;

export const fromHeroesActions = {
  LoadHeroes,
  HeroesLoaded,
  HeroesLoadError,
  AddHero,
  HeroAddedError,
  DeleteHero,
  HeroDeletedError,
  SearchHeroes,
  HeroesSearched,
  HeroesSearchedError,
};
