import { createFeatureSelector, createSelector } from '@ngrx/store';
import { HEROES_FEATURE_KEY, HeroesState } from './heroes.reducer';

// Lookup the 'Heroes' feature state managed by NgRx
const getHeroesState = createFeatureSelector<HeroesState>(HEROES_FEATURE_KEY);

const getLoaded = createSelector(
  getHeroesState,
  (state: HeroesState) => state.loaded
);
const getError = createSelector(
  getHeroesState,
  (state: HeroesState) => state.error
);

const getAllHeroes = createSelector(
  getHeroesState,
  getLoaded,
  (state: HeroesState, isLoaded) => {
    return isLoaded ? state.list : [];
  }
);
const getSelectedId = createSelector(
  getHeroesState,
  (state: HeroesState) => state.selectedId
);
const getSelectedHeroes = createSelector(
  getAllHeroes,
  getSelectedId,
  (heroes, id) => {
    const result = heroes.find(it => it['id'] === id);
    return result ? Object.assign({}, result) : undefined;
  }
);

export const heroesQuery = {
  getLoaded,
  getError,
  getAllHeroes,
  getSelectedHeroes
};
