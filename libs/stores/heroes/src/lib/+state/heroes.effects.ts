import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';

import { HeroesPartialState } from './heroes.reducer';
import {
  LoadHeroes,
  HeroesLoaded,
  HeroesLoadError,
  HeroesActionTypes,
  AddHero,
  HeroAddedError,
  DeleteHero,
  HeroDeletedError
} from './heroes.actions';
import { HeroService, Hero } from '@tour-of-heroes/backends';
import { map } from 'rxjs/operators';

@Injectable()
export class HeroesEffects {
  @Effect() loadHeroes$ = this.dataPersistence.fetch(
    HeroesActionTypes.LoadHeroes,
    {
      run: (action: LoadHeroes, state: HeroesPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        return this.heroService.getHeroes().pipe(
          map(heroes => {
            return new HeroesLoaded(heroes);
          })
        )
      },

      onError: (action: LoadHeroes, error) => {
        console.error('Error', error);
        return new HeroesLoadError(error);
      }
    }
  );

  @Effect() addHero$ = this.dataPersistence.fetch(
    HeroesActionTypes.AddHero,
    {
      run: (action: AddHero, state: HeroesPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        return this.heroService.addHero(action.payload as Hero).pipe(
          map(hero => {
            return new LoadHeroes();
          })
        )
      },

      onError: (action: AddHero, error) => {
        console.error('Error', error);
        return new HeroAddedError(error);
      }
    }
  );

  @Effect() deleteHero$ = this.dataPersistence.fetch(
    HeroesActionTypes.DeleteHero,
    {
      run: (action: DeleteHero, state: HeroesPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        return this.heroService.deleteHero(action.payload as Hero).pipe(
          map(hero => {
            return new LoadHeroes();
          })
        )
      },

      onError: (action: DeleteHero, error) => {
        console.error('Error', error);
        return new HeroDeletedError(error);
      }
    }
  );

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<HeroesPartialState>,
    private heroService: HeroService
  ) {}
}
