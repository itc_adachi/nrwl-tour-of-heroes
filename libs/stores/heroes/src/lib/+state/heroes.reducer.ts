import { HeroesAction, HeroesActionTypes } from './heroes.actions';

export const HEROES_FEATURE_KEY = 'heroes';

/**
 * Interface for the 'Heroes' data used in
 *  - HeroesState, and
 *  - heroesReducer
 *
 *  Note: replace if already defined in another module
 */

/* tslint:disable:no-empty-interface */
export interface Entity {}

export interface HeroesState {
  list: Entity[]; // list of Heroes; analogous to a sql normalized table
  selectedId?: string | number; // which Heroes record has been selected
  loaded: boolean; // has the Heroes list been loaded
  error?: any; // last none error (if any)
}

export interface HeroesPartialState {
  readonly [HEROES_FEATURE_KEY]: HeroesState;
}

export const initialState: HeroesState = {
  list: [],
  loaded: false
};

export function heroesReducer(
  state: HeroesState = initialState,
  action: HeroesAction
): HeroesState {
  switch (action.type) {
    case HeroesActionTypes.HeroesLoaded: {
      state = {
        ...state,
        list: action.payload,
        loaded: true
      };
      break;
    }
  }
  return state;
}
