import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/nx/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/nx';

import { HeroesEffects } from './heroes.effects';
import { HeroesFacade } from './heroes.facade';

import { heroesQuery } from './heroes.selectors';
import { LoadHeroes, HeroesLoaded } from './heroes.actions';
import {
  HeroesState,
  Entity,
  initialState,
  heroesReducer
} from './heroes.reducer';

interface TestSchema {
  heroes: HeroesState;
}

describe('HeroesFacade', () => {
  let facade: HeroesFacade;
  let store: Store<TestSchema>;
  let createHeroes;

  beforeEach(() => {
    createHeroes = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
  });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature('heroes', heroesReducer, { initialState }),
          EffectsModule.forFeature([HeroesEffects])
        ],
        providers: [HeroesFacade]
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule
        ]
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.get(Store);
      facade = TestBed.get(HeroesFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async done => {
      try {
        let list = await readFirst(facade.allHeroes$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.loadAll();

        list = await readFirst(facade.allHeroes$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `HeroesLoaded` to manually submit list for state management
     */
    it('allHeroes$ should return the loaded list; and loaded flag == true', async done => {
      try {
        let list = await readFirst(facade.allHeroes$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        store.dispatch(
          new HeroesLoaded([createHeroes('AAA'), createHeroes('BBB')])
        );

        list = await readFirst(facade.allHeroes$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
});
