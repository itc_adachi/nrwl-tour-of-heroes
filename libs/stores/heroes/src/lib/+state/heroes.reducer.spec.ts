import { HeroesLoaded } from './heroes.actions';
import {
  HeroesState,
  Entity,
  initialState,
  heroesReducer
} from './heroes.reducer';

describe('Heroes Reducer', () => {
  const getHeroesId = it => it['id'];
  let createHeroes;

  beforeEach(() => {
    createHeroes = (id: string, name = ''): Entity => ({
      id,
      name: name || `name-${id}`
    });
  });

  describe('valid Heroes actions ', () => {
    it('should return set the list of known Heroes', () => {
      const heroess = [
        createHeroes('PRODUCT-AAA'),
        createHeroes('PRODUCT-zzz')
      ];
      const action = new HeroesLoaded(heroess);
      const result: HeroesState = heroesReducer(initialState, action);
      const selId: string = getHeroesId(result.list[1]);

      expect(result.loaded).toBe(true);
      expect(result.list.length).toBe(2);
      expect(selId).toBe('PRODUCT-zzz');
    });
  });

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = heroesReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
