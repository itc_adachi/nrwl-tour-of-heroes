import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { HeroesPartialState } from './heroes.reducer';
import { heroesQuery } from './heroes.selectors';
import { LoadHeroes, AddHero, DeleteHero } from './heroes.actions';
import { Hero } from '@tour-of-heroes/backends';

@Injectable()
export class HeroesFacade {
  loaded$ = this.store.pipe(select(heroesQuery.getLoaded));
  allHeroes$ = this.store.pipe(select(heroesQuery.getAllHeroes));
  selectedHeroes$ = this.store.pipe(select(heroesQuery.getSelectedHeroes));

  constructor(private store: Store<HeroesPartialState>) {}

  loadAll() {
    this.store.dispatch(new LoadHeroes());
  }

  addHero(hero: Hero) {
    this.store.dispatch(new AddHero(hero));
  }

  deleteHero(hero: Hero) {
    this.store.dispatch(new DeleteHero(hero));
  }
}
