export * from './lib/+state/heroes.facade';
export * from './lib/+state/heroes.reducer';
export * from './lib/+state/heroes.selectors';
export * from './lib/stores-heroes.module';
