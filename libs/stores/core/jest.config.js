module.exports = {
  name: 'stores-core',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/stores/core'
};
