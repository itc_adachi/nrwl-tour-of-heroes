import { async, TestBed } from '@angular/core/testing';
import { StoresCoreModule } from './stores-core.module';

describe('StoresCoreModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoresCoreModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(StoresCoreModule).toBeDefined();
  });
});
