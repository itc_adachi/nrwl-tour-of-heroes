import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesBodyComponent } from './heroes-body.component';

describe('HeroesBodyComponent', () => {
  let component: HeroesBodyComponent;
  let fixture: ComponentFixture<HeroesBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
