import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '@tour-of-heroes/backends';

@Component({
  selector: 'tour-of-heroes-org-heroes-body',
  templateUrl: './heroes-body.component.html',
  styleUrls: ['./heroes-body.component.scss']
})
export class HeroesBodyComponent implements OnInit {

  @Input() heroes: Hero[];
  @Output() addEvent: EventEmitter<{ name: string }> = new EventEmitter<{ name: string }>();
  @Output() deleteEvent: EventEmitter<{ hero: Hero }> = new EventEmitter<{ hero: Hero }>();

  constructor() { }

  ngOnInit() {
  }

  add(name: string): void {
    this.addEvent.emit({ name: name });
  }

  delete($event): void {
    this.deleteEvent.emit($event);
  }
}
