import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentsMoleculesModule } from '@tour-of-heroes/components/molecules';
import { DashboardBodyComponent } from './dashboard-body/dashboard-body.component';
import { HeroesBodyComponent } from './heroes-body/heroes-body.component';
import { ComponentsAtomsModule } from '@tour-of-heroes/components/atoms';
import { MessagesComponent } from './messages/messages.component';

@NgModule({
  imports: [CommonModule, RouterModule, ComponentsMoleculesModule, ComponentsAtomsModule],
  declarations: [DashboardBodyComponent, HeroesBodyComponent, MessagesComponent],
  exports: [DashboardBodyComponent, HeroesBodyComponent, MessagesComponent],
})
export class ComponentsOrganismsModule {}
