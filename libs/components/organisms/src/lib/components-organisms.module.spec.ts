import { async, TestBed } from '@angular/core/testing';
import { ComponentsOrganismsModule } from './components-organisms.module';

describe('ComponentsOrganismsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ComponentsOrganismsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ComponentsOrganismsModule).toBeDefined();
  });
});
