import { Component, OnInit } from '@angular/core';
import { MessageService } from '@tour-of-heroes/features';

@Component({
  selector: 'tour-of-heroes-org-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor(public messageService: MessageService) { }

  ngOnInit() {
  }

}
