import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '@tour-of-heroes/backends';

@Component({
  selector: 'tour-of-heroes-org-dashboard-body',
  templateUrl: './dashboard-body.component.html',
  styleUrls: ['./dashboard-body.component.scss']
})
export class DashboardBodyComponent implements OnInit {

  @Input() topHeroes: Hero[];

  constructor() { }

  ngOnInit() {
  }
}
