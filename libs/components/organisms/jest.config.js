module.exports = {
  name: 'components-organisms',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/components/organisms'
};
