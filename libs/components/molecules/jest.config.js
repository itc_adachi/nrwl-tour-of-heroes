module.exports = {
  name: 'components-molecules',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/components/molecules'
};
