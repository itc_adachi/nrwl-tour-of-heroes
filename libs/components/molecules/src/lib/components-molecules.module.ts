import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TopHeroesComponent } from './top-heroes/top-heroes.component';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { ComponentsAtomsModule } from '@tour-of-heroes/components/atoms';
import { HeroesListComponent } from './heroes-list/heroes-list.component';

@NgModule({
  imports: [CommonModule, RouterModule, ComponentsAtomsModule],
  declarations: [TopHeroesComponent, HeroSearchComponent, HeroesListComponent],
  exports: [TopHeroesComponent, HeroSearchComponent, HeroesListComponent],
})
export class ComponentsMoleculesModule {}
