import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '@tour-of-heroes/backends';

@Component({
  selector: 'tour-of-heroes-mol-top-heroes',
  templateUrl: './top-heroes.component.html',
  styleUrls: ['./top-heroes.component.scss']
})
export class TopHeroesComponent implements OnInit {

  @Input() heroes: Hero[];

  constructor() { }

  ngOnInit() {
  }

}
