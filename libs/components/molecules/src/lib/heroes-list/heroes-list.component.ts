import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '@tour-of-heroes/backends';

@Component({
  selector: 'tour-of-heroes-mol-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.scss']
})
export class HeroesListComponent implements OnInit {

  @Input() heroes: Hero[];
  @Output() deleteEvent: EventEmitter<{ hero: Hero }> = new EventEmitter<{ hero: Hero }>();

  constructor() { }

  ngOnInit() {
  }

  delete($event): void {
    this.deleteEvent.emit($event);
  }
}
