import { async, TestBed } from '@angular/core/testing';
import { ComponentsMoleculesModule } from './components-molecules.module';

describe('ComponentsMoleculesModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ComponentsMoleculesModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ComponentsMoleculesModule).toBeDefined();
  });
});
