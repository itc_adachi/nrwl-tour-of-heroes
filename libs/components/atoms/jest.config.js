module.exports = {
  name: 'components-atoms',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/components/atoms'
};
