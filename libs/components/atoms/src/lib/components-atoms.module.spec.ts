import { async, TestBed } from '@angular/core/testing';
import { ComponentsAtomsModule } from './components-atoms.module';

describe('ComponentsAtomsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ComponentsAtomsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ComponentsAtomsModule).toBeDefined();
  });
});
