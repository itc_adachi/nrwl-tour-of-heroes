import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopHeroesTitleComponent } from './top-heroes-title.component';

describe('TopHeroesTitleComponent', () => {
  let component: TopHeroesTitleComponent;
  let fixture: ComponentFixture<TopHeroesTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopHeroesTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopHeroesTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
