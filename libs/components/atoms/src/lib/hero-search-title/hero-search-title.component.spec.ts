import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSearchTitleComponent } from './hero-search-title.component';

describe('HeroSearchTitleComponent', () => {
  let component: HeroSearchTitleComponent;
  let fixture: ComponentFixture<HeroSearchTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroSearchTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroSearchTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
