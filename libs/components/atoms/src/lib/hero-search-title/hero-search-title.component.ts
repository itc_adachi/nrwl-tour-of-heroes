import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tour-of-heroes-atm-hero-search-title',
  templateUrl: './hero-search-title.component.html',
  styleUrls: ['./hero-search-title.component.scss']
})
export class HeroSearchTitleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
