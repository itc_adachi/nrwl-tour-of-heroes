import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesTitleComponent } from './heroes-title.component';

describe('HeroesTitleComponent', () => {
  let component: HeroesTitleComponent;
  let fixture: ComponentFixture<HeroesTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
