import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HeroService, Hero } from '@tour-of-heroes/backends';

@Component({
  selector: 'tour-of-heroes-atm-hero-add',
  templateUrl: './hero-add.component.html',
  styleUrls: ['./hero-add.component.scss']
})
export class HeroAddComponent implements OnInit {

  @Output() addEvent: EventEmitter<string> = new EventEmitter<string>();

  add(name: string): void {
    name = name.trim();
    this.addEvent.emit(name);
  }

  constructor(private heroService: HeroService) { }

  ngOnInit() {
  }
}
