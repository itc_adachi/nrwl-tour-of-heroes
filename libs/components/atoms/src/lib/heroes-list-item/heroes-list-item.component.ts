import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero, HeroService } from '@tour-of-heroes/backends';

@Component({
  selector: 'tour-of-heroes-atm-heroes-list-item',
  templateUrl: './heroes-list-item.component.html',
  styleUrls: ['./heroes-list-item.component.scss']
})
export class HeroesListItemComponent implements OnInit {

  @Input() heroes: Hero[];
  @Output() deleteEvent: EventEmitter<{ hero: Hero }> = new EventEmitter<{ hero: Hero }>();

  constructor(private heroService: HeroService) { }

  ngOnInit() {
  }

  delete(hero: Hero): void {
    this.deleteEvent.emit({ hero: hero });
    // this.heroes = this.heroes.filter(h => h !== hero);
    // this.heroService.deleteHero(hero).subscribe();
  }
}
