import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopHeroesTitleComponent } from './top-heroes-title/top-heroes-title.component';
import { TopHeroComponent } from './top-hero/top-hero.component';
import { HeroSearchTitleComponent } from './hero-search-title/hero-search-title.component';
import { HeroSearchBoxComponent } from './hero-search-box/hero-search-box.component';
import { RouterModule } from '@angular/router';
import { HeroesTitleComponent } from './heroes-title/heroes-title.component';
import { HeroesListItemComponent } from './heroes-list-item/heroes-list-item.component';
import { HeroAddComponent } from './hero-add/hero-add.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [TopHeroesTitleComponent, TopHeroComponent, HeroSearchTitleComponent, HeroSearchBoxComponent, HeroesTitleComponent, HeroesListItemComponent, HeroAddComponent],
  exports: [TopHeroesTitleComponent, TopHeroComponent, HeroSearchTitleComponent, HeroSearchBoxComponent, HeroesTitleComponent, HeroesListItemComponent, HeroAddComponent],
})
export class ComponentsAtomsModule {}
