import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '@tour-of-heroes/backends';

@Component({
  selector: 'tour-of-heroes-atm-top-hero',
  templateUrl: './top-hero.component.html',
  styleUrls: ['./top-hero.component.scss']
})
export class TopHeroComponent implements OnInit {

  @Input() heroes: Hero;

  constructor() { }

  ngOnInit() {
  }

}
