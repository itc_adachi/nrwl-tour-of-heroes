import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tour-of-heroes-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.css']
})
export class AppShellComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
