import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NxModule } from '@nrwl/nx';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { InMemoryDataService } from '@tour-of-heroes/backends';
import { ComponentsOrganismsModule } from '@tour-of-heroes/components/organisms';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    NxModule.forRoot(),
    ComponentsOrganismsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {
      dataEncapsulation: false
    }),
    RouterModule.forRoot(
      [
        { path: '', redirectTo: '/contents-dashboard', pathMatch: 'full' },
        {
          path: 'contents-heroes',
          loadChildren: '@tour-of-heroes/contents/heroes#ContentsHeroesModule'
        },
        {
          path: 'contents-hero-detail',
          loadChildren:
            '@tour-of-heroes/contents/hero-detail#ContentsHeroDetailModule'
        },
        {
          path: 'contents-dashboard',
          loadChildren:
            '@tour-of-heroes/contents/dashboard#ContentsDashboardModule'
        }
      ],
      { initialNavigation: 'enabled' }
    ),
    StoreModule.forRoot(
      {},
      { metaReducers: !environment.production ? [storeFreeze] : [] }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
