import { Component } from '@angular/core';

@Component({
  selector: 'tour-of-heroes-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'nx-toh';
}
